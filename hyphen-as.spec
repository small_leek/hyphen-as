Name:           hyphen-as
Summary:        Assamese hyphenation rules
Version:        0.7.0
Release:        13
Epoch:          1
Source:         http://download.savannah.gnu.org/releases/smc/hyphenation/patterns/hyphen-as-%{version}.tar.bz2
URL:            http://wiki.smc.org.in
License:        LGPLv3+
BuildArch:      noarch
Requires:       hyphen
Supplements:    (hyphen and langpacks-as)

%description
The package provide hyphenation rules for Assamese language.

%prep
%autosetup -n hyphen-as-%{version} -p1 -S git

%build

%install
install -Dp hyph_as_IN.dic %{buildroot}/%{_datadir}/hyphen/hyph_as_IN.dic
chmod 644  %{buildroot}/%{_datadir}/hyphen/*.dic

%files
%doc README COPYING ChangeLog
%{_datadir}/hyphen/*

%changelog
* Fri Apr 17 2020 Jeffery.Gao <gaojianxing@huawei.com> - 1:0.7.0-13
- Package init
